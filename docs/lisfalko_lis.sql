-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 09/07/2014 às 17:49
-- Versão do servidor: 5.1.73-cll
-- Versão do PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `lisfalko_lis`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `descricao` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `categoria`
--

INSERT INTO `categoria` (`id`, `nome`, `descricao`) VALUES
(1, 'Acadêmicos', NULL),
(2, 'Interiores', NULL),
(3, 'Residenciais', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `imagem`
--

CREATE TABLE IF NOT EXISTS `imagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) DEFAULT NULL,
  `descricao` text,
  `arquivo` varchar(120) DEFAULT NULL,
  `extensao` varchar(5) DEFAULT NULL,
  `video` varchar(120) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `projeto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_imagem_projeto1_idx` (`projeto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Fazendo dump de dados para tabela `imagem`
--

INSERT INTO `imagem` (`id`, `nome`, `descricao`, `arquivo`, `extensao`, `video`, `ordem`, `projeto`) VALUES
(5, 'Implantação', NULL, 'o_18rso4hubpu6hqa19fo1hpm70a', 'png', NULL, 2, 1),
(6, 'Plantas Baixas', NULL, 'o_18rso4hub1jkr19f22cu198ub3cb', 'png', NULL, 3, 1),
(7, 'Croqui', NULL, 'o_18rso4hubfim1j2ptbq1mck1p9lc', 'png', NULL, 1, 1),
(9, 'Croqui', NULL, 'o_18rsofuavadd117rr6106e3ot7', 'png', NULL, 0, 1),
(10, 'Planta Baixa - 1º Pav. Duplex', NULL, 'o_18rsojvdf6e61lq91m6njeujski', 'jpg', NULL, 4, 2),
(11, 'Planta Baixa - 2º Pav. Duplex', NULL, 'o_18rsojvdfi6a1c5q18641fnb1c6tj', 'jpg', NULL, 5, 2),
(12, 'Planta Baixa - Aps. Acessiveis', NULL, 'o_18rsojvdf1iqo1r88120qkai1d05k', 'jpg', NULL, 3, 2),
(13, 'Corte de Pele - A', NULL, 'o_18rsojvdf1bsh1rp1eml1832s9l', 'jpg', NULL, 9, 2),
(14, 'Corte de Pele - B', NULL, 'o_18rsojvdf10l09nt5a9mk72rnm', 'jpg', NULL, 10, 2),
(15, 'Corte de pele - C', NULL, 'o_18rsojvdfsik1q361fv622jofln', 'jpg', NULL, 11, 2),
(16, 'Cortes esquemáticos', NULL, 'o_18rsojvdf4p7bgn1n8e1c96bj9o', 'jpg', NULL, 7, 2),
(17, 'Cortes', NULL, 'o_18rsojvdg1phd1iu1eub1fghdpep', 'jpg', NULL, 8, 2),
(18, 'Fachadas', NULL, 'o_18rsojvdg1nrc1sle1esp3o46jq', 'jpg', NULL, 6, 2),
(19, 'Croqui', NULL, 'o_18rsojvdg1m921urscf6ft6degr', 'jpg', NULL, 0, 2),
(20, 'Implantação', NULL, 'o_18rsojvdg9cf1spfvt8ejm12es', 'jpg', NULL, 2, 2),
(21, 'Croqui', NULL, 'o_18rsojvdgkur111e1rme2i9pput', 'jpg', NULL, 1, 2),
(22, 'Fachadas', NULL, 'o_18rsp7nduhb1vl511rv7k0d28e', 'png', NULL, 3, 3),
(23, 'Implantação', NULL, 'o_18rsp7ndu1ju8125rb9ac3a4h3f', 'png', NULL, 1, 3),
(24, 'Plantas Baixas', NULL, 'o_18rsp7ndu18e3p7ggrbl5g1ubrg', 'png', NULL, 2, 3),
(25, 'Capa', NULL, 'o_18rsp7nduso1ih71ch81kc4u0hh', 'jpg', NULL, 0, 3),
(26, 'Croqui', NULL, 'o_18rsp7ndurus11ul1sa51ur411sai', 'jpg', NULL, 4, 3),
(27, 'Croqui', NULL, 'o_18rsp7ndu1pr11vvb1b7e5na1h72j', 'jpg', NULL, 5, 3),
(28, 'Croqui', NULL, 'o_18rsp7ndujg5aea1saahgh11lok', 'jpg', NULL, 6, 3),
(29, 'Croqui', NULL, 'o_18rsp7ndu9q131hg431ig87j5l', 'jpg', NULL, 7, 3),
(30, 'Paisagismo - Croqui', NULL, 'o_18rsp7vpc1nf6cl9jn8j08788t', 'jpg', NULL, 11, 3),
(31, 'Paisagismo - Croqui', NULL, 'o_18rsp7vpcj9r1a3i1jgq1h1hrj9u', 'jpg', NULL, 12, 3),
(32, 'Paisagismo - Croqui', NULL, 'o_18rsp7vpcarsh16q451lu51i99v', 'jpg', NULL, 10, 3),
(33, 'Paisagismo - Croqui', NULL, 'o_18rsp7vpc6oj1fqa24m41cloh10', 'jpg', NULL, 13, 3),
(34, 'Paisagismo - Croqui', NULL, 'o_18rsp7vpc1e7i14ao1ta917sf1afk11', 'jpg', NULL, 14, 3),
(35, 'Paisagismo - Planta Baixa', NULL, 'o_18rspi6leqcdn651poi1r8f10rt8', 'jpg', NULL, 8, 3),
(36, 'Paisagismo - Planta Estações', NULL, 'o_18rspi6le1frmqiv1n3011b81q9q9', 'jpg', NULL, 9, 3),
(37, 'Planta de Cobertura', NULL, 'o_18rsqgbue1m7c2bj1dge9qh13rkq', 'jpg', NULL, 4, 4),
(38, 'Cortes', NULL, 'o_18rsqgbuf14f9lt9flo1p181p04r', 'jpg', NULL, 8, 4),
(39, 'Detalhamentos', NULL, 'o_18rsqgbuf24211mlsrklvdavus', 'jpg', NULL, 11, 4),
(40, 'Evolução', NULL, 'o_18rsqgbufde2e65v031ofnghat', 'png', NULL, 1, 4),
(41, 'Fachadas', NULL, 'o_18rsqgbuf1tna1jjtb9l3d81ka3u', 'jpg', NULL, 9, 4),
(42, 'Fachadas', NULL, 'o_18rsqgbuf1opg1pn8n50331d2mv', 'png', NULL, 10, 4),
(43, 'Croqui', NULL, 'o_18rsqgbuftgh1r8pbe5f60167310', 'jpg', NULL, 13, 4),
(44, 'Croqui', NULL, 'o_18rsqgbufeqk1sbk6vs6i1vef11', 'jpg', NULL, 15, 4),
(45, 'Croqui', NULL, 'o_18rsqgbuf1uq3ls7a4nmkvukg12', 'jpg', NULL, 16, 4),
(46, 'Croqui', NULL, 'o_18rsqgbufuek5s012bnp841k1o13', 'jpg', NULL, 14, 4),
(47, 'Implantação', NULL, 'o_18rsqgbuf1k63q7h1l451jg7112914', 'jpg', NULL, 12, 4),
(48, 'Croqui', NULL, 'o_18rsqgbuf160rh0t1361t4oiu15', 'jpg', NULL, 17, 4),
(49, 'Croqui', NULL, 'o_18rsqgbufmdu5ec1rqhsh51hdv16', 'jpg', NULL, 18, 4),
(50, 'Croqui', NULL, 'o_18rsqgbuf1hirgl12lfd001mla17', 'jpg', NULL, 0, 4),
(51, 'Croqui', NULL, 'o_18rsqgbufli9t811uug10ef9g618', 'jpg', NULL, 19, 4),
(52, 'Planta Baixa - Mezanino', NULL, 'o_18rsqgbufdjo1tgiig115ft53v19', 'jpg', NULL, 7, 4),
(53, 'Planta Baixa - Subsolo', NULL, 'o_18rsqgbuf9nh17g78v31egi1k1k1a', 'jpg', NULL, 5, 4),
(54, 'Planta Baixa - Térreo', NULL, 'o_18rsqgbuf10skq421ivh10elvci1b', 'jpg', NULL, 6, 4),
(55, 'Funcionograma', NULL, 'o_18rsqgbufdk51bj71v4q1p2q2f91c', 'png', NULL, 2, 4),
(56, 'Zoneamento', NULL, 'o_18rsqgbug17vl14cnvpg1s981onu1d', 'png', NULL, 3, 4),
(57, 'Jukebox - Shows e Eventos Musicais', NULL, 'https://i1.ytimg.com/vi/MAHMXmJhmfE/hqdefault.jpg', NULL, 'https://www.youtube.com/watch?v=MAHMXmJhmfE', 20, 4);

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(120) DEFAULT NULL,
  `descricao` text,
  `cidade` varchar(120) DEFAULT NULL,
  `cau` varchar(45) DEFAULT NULL,
  `telefone` varchar(14) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `perfil`
--

INSERT INTO `perfil` (`id`, `titulo`, `descricao`, `cidade`, `cau`, `telefone`, `email`) VALUES
(1, 'Arquiteta e Urbanista', 'Arquiteta formada no Centro Universitário Franciscano, em Santa Maria. Atualmente matriculada no curso de Pós Graduação Master em Arquitetura e Iluminação do IPOG.', 'Santa Maria - RS', 'A96025-0', '(55) 9661-2783', 'lis.falkowski@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projeto`
--

CREATE TABLE IF NOT EXISTS `projeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `descricao` text,
  `texto` text,
  `data` date DEFAULT NULL,
  `categoria` int(11) NOT NULL,
  `ativo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_projeto_categoria_idx` (`categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Fazendo dump de dados para tabela `projeto`
--

INSERT INTO `projeto` (`id`, `nome`, `descricao`, `texto`, `data`, `categoria`, `ativo`) VALUES
(1, 'AT I - Equipamentos Comunitários', 'Projeto de uma estação rodoviária para a cidade de Santa Maria localizada em Camobi juntamente com um centro comunitário.', NULL, NULL, 1, 1),
(2, 'AT II - Verticalização Urbana', '<b>Projeto Urbano</b> - Vila Olímpica (Porto Alegre)<br><b>Projeto Arquitetônico</b> - Alojamento das diversas delegações que participam dos jogos olímpicos.', '<b><b>Conceito</b><br></b>As olimpíadas quando surgiram eram feitas em \r\nhomenagem ao deus grego zeus, o qual possui como símbolos o raio e a \r\náguia. para o desenvolvimento da forma do edifício foi tomado como base o\r\n desenho de um raio, pois além de ser o símbolo de zeus ele também \r\ntransmite uma imagem de força e rapidez, que são características \r\nnecessárias aos atletas em várias modalidades das olimpíadas.<br>Além \r\ndisso, para a concepção formal do edifício levou-se em consideração o \r\nmeio em que ele está inserido (a orla do guaíba), o que fez com que \r\ntodas as unidades fossem posicionadas de maneira a possuírem visuais \r\npara o guaíba, além de uma ótima ventilação e fenestrações (nos duplex) \r\npara duas direções.<b><br><br>Memorial descritivo</b><br>Levando em consideração que também serão efetuados jogos paraolímpicos, faz-se necessária a existência de unidades acessíveis à portadores de necessidades especiais (que se localizam nos seis primeiros pavimentos). O restante das unidades residenciais foi proposta em forma de duplex, para que assim os apartamentos obtivessem uma boa orientação solar e ventilação.<br><br>A edificação foi concebida em quatro blocos, os quais são ligados através de núcleos de circulação vertical. O acesso a esses blocos se da através de duas recepções, uma para cada dois blocos. A circulação horizontal existe a cada dois andares (nos andares que possuem duplex), o que faz com que existam vazios ao longo dos blocos, o que ocasiona uma quebra na monumentalidade da edificação.<br><br>Outro artifício utilizado para essa quebra é o seu revestimento, o mesmo foi feito em um degrade do cinza ao preto, com pedra basalto de duas tonalidades e tinta cinza. Para diferenciar as unidades residenciais das subtrações feitas no volume é utilizado o recurso da cor amarela onde existem essas fenestrações, assim como houve uma separação dos andares dos apartamentos acessíveis dos demais, dando uma diferenciação aos olhos do observador.<br><br>Para hierarquizar os acessos, a edificação foi elevada sob pilotis, existindo no térreo somente os blocos de circulação vertical e as unidades das delegações (que por possuírem salas médicas foram posicionadas no térreo, para um melhor acesso de ambulâncias e veículos de emergência).<br><br>Para a estrutura do edifício serão utilizados pilares metálicos (de 40cmx40xm) e laje nervurada (espessura = 35cm), o que permite uma maior flexibilidade de layout, pois como o uso pós-olímpiadas será residencial, vai existir a possibilidade de uma concepção de vários tipos de apartamentos.<br><br>Para atender as rotas de fuga exigidas pela norma 9077, foram colocadas 7 escadas a prova de fumaça e faz-se o uso de sprinklers para que a distância máxima a ser percorrida seja de 45 metro e possa ser atingida em todos os pontos da edificação.<br><br>', NULL, 1, 1),
(3, 'AT III - Intervenções em Pré-Existências', 'Projeto de intervenção na antiga Estação Colônia de Camobi.', '<span>Como proposta para o projeto arquitetônico toma-se como base uma edificação de grande importância da cidade de Santa Maria, a sede da antiga Estação Colônia de Camobi, que devido ao desuso da linha férrea acabou sendo esquecida no tempo e se degradando. Com o objetivo de resgatar essa parte da história de Santa Maria, propõe-se a reativação da linha férrea para a locomoção de um trem turístico, que faça conexão com Itaara e cidades da Quarta Colônia, o que atrairá visitantes para a região, além de proporcionar o desenvolvimento das cidades envolvidas. Além da reinstalação de uma estação ferroviária, essa intervenção propõe novos usos que darão destaque para a área e servirão como pontos de atração para a edificação. Esses usos incluem um memorial ferroviário, para que os visitantes conheçam a história por trás da edificação; um restaurante/bar que funcionará tanto de dia quanto de noite, para que não ocorram turnos ociosos na instalação; uma biblioteca, pois é um equipamento que está em falta nessa região da cidade; e uma sala multiuso que pode ser utilizada para eventos e palestras.<br><br></span>Como a estação se desenvolve de um lado dos trilhos e a cidade de outro, surge a necessidade da criação de uma passagem de pedestres sobre os trilhos que não atrapalhe a locomoção do trem. Essa passagem se dará em forma de passarela que iniciará no nível da Av. João Machado Soares, já contendo a inclinação necessária para ser acessível à PNE’s e chegará no nível da cobertura da edificação, fazendo a união entre o antigo e o novo. Para que os usuários desçam para o nível da estação optou-se pelo uso de um elevador, pois uma rampa seria muito longa e de alto custo. Para a organização do espaço, foi criado um eixo visual entre a parte principal da pré-existência e a igreja que localiza-se no terreno em frente. Parte da edificação antiga que funcionava como armazém foi destinada ao funcionamento do memorial ferroviário enquanto a parte que antigamente era a estação teve o seu uso resgatado. Para criar uma relação entre as edificações, a volumetria da estação foi rebatida e utilizada para concentrar os outros ambientes necessários da estação. Neste novo volume optou-se por um pano de vidro na face imediatamente em frente a antiga estação, para que essa fachada continue tendo importância, além dos usuários que estão no café da estação possuírem essa visual.<br><br>', NULL, 1, 1),
(4, 'TFG II - JUKEBOX Shows e Eventos Musicais', 'Projeto do Trabalho Final de Graduação II, onde o tema proposto é uma casa de shows denominada Jukebox e localizada na cidade de Santa Maria.', '<span><b>Apresentação do tema<br></b><br>Santa Maria é considerada uma cidade universitária por possuir um grande número de instituições de ensino e, por isso, concentra um grande número de jovens estudantes. A existência de um público que cobiça um local para a música e sua localização no centro do estado fazem com que a cidade seja sede de muitos eventos culturais e musicais. Porém, ainda não existe um local de qualidade e direcionado exclusivamente para este uso, onde fatores como acústica, acessibilidade, conforto e segurança dos usuários sejam levados em consideração. <br><br>Com isso, surge a proposta de um espaço de qualidade que atenda a demanda do público existente na cidade e tenha toda a estrutura imprescindível a este tipo de evento, o qual será composto de uma arena de shows com todo o aparato técnico necessário e contará com acesso separado para os músicos e bandas, para assim garantir uma maior tranquilidade a estes profissionais. O equipamento também possuirá um espaço destinado a realização de pocket shows, onde o número de espectadores é menor e terá o intuito de dar apoio às bandas locais, para que estas também possuam um local de qualidade para divulgar o seu trabalho. <br><br>O mesmo ainda será composto de um espaço ao ar livre, pois a inserção de espaços abertos junto ao equipamento gera possibilidades de utilização além dos shows em ambientes fechados, como a realização de festivais de música (muito presentes em Santa Maria) e outras manifestações culturais.<br><br><b>Terreno escolhido<br></b><br>A área de intervenção está localizada na Rua Gov. Walter Jobim, esquina com a Rua Antônio Moraes da Cunha, no bairro Patronato, em Santa Maria. A escolha do sítio deu-se a partir da consideração que o público alvo é, em sua maioria, estudantes que utilizam muito o transporte coletivo e, por isso, faz-se necessária a inserção do equipamento em um terreno próximo ao centro da cidade que possibilite a utilização deste tipo de transporte, além de transportes de aluguel e a viabilização de caronas, fazendo com que os usuários possam aproveitar ao máximo os espetáculos e voltar para casa em segurança.</span>', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `lastlogin`) VALUES
(1, 'lisfalko', 'aa1bf4646de67fd9086cf6c79007026c', '2014-07-02 22:46:55');

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `imagem`
--
ALTER TABLE `imagem`
  ADD CONSTRAINT `fk_imagem_projeto1` FOREIGN KEY (`projeto`) REFERENCES `projeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `projeto`
--
ALTER TABLE `projeto`
  ADD CONSTRAINT `fk_projeto_categoria` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
