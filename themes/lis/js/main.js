$(document).ready(function() {
    $('#filtro-projetos').find('.filtro-item').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');

            $('#projetos').find('.item').each(function() {
                $(this).removeClass('hide');
                $(this).show().animate({
                    opacity: 1
                }, 300);
            });

        } else {
            categoria = $(this).data('categoria');
            
            $('#filtro-projetos').find('.active').removeClass('active');
            $(this).addClass('active');

            $('#projetos').find('.item[data-categoria=' + categoria + ']').each(function() {
                $(this).removeClass('hide');
                $(this).animate({
                    opacity: 1
                }, 300, function() {
                    $(this).show();
                });
            });
            
            $('#projetos').find('.item').not('[data-categoria=' + categoria + ']').each(function() {
                $(this).addClass('hide');
                $(this).animate({
                    opacity: 0
                }, 300, function() {
                    $(this).hide();
                });
            });
        }
    });
    
    $('#projetos').find('.item').on('click', function() {
        if ($('#filtro-projetos').find('.active').length) {
            var urlArr = $(this).attr('href').split('/');
            var id = urlArr.pop();
            var cat = $(this).data('nome');
            var url = urlArr.join('/') + '/' + cat + '/' + id;
            $(this).attr('href', url);
        }
        
        return true;
    });
});