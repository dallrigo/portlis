<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="pt-BR" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="arquitetura, arquiteto, arquiteta, design, urbanismo, paisagismo, interiores, decoração, projeto, lis, lis falkowski, lisfalkowski, portfolio, santa maria, encantado, lajeado" />
        <meta name="description" content="Arquiteta formada no Centro Universitário Franciscano, em Santa Maria. Atualmente matriculada no curso de Pós Graduação Master em Arquitetura e Iluminação do IPOG." />

        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/icon/css/fontello.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/icon/css/animation.css" />

        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/main.js"); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/galleria/galleria-1.3.5.min.js"); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/galleria/themes/classic/galleria.classic.min.js"); ?>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <?php if (!Yii::app()->user->isGuest) { ?>
            <div id="admin-header">
                <?php
                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'brand' => 'Painel',
                    'type' => 'inverse',
                    'fixed' => 'top',
                    'brandOptions' => array(
                        'class' => 'visible-phone visible-tablet'
                    ),
                    'fixed' => false,
                    'collapse' => true,
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'items' => array(
                                array('label' => 'Home', 'url' => Yii::app()->request->getBaseUrl(true), 'itemOptions' => array('class' => 'visible-desktop')),
                                array('label' => 'Perfil', 'items' => array(
                                        array('label' => 'Gerenciar', 'url' => Yii::app()->request->getBaseUrl(true) . '/painel/perfil/update'),
                                    )),
                                array('label' => 'Projetos', 'items' => array(
                                        array('label' => 'Incluir', 'url' => Yii::app()->request->getBaseUrl(true) . '/painel/projeto/create'),
                                        array('label' => 'Gerenciar', 'url' => Yii::app()->request->getBaseUrl(true) . '/painel/projeto/admin')
                                    )),
                            )
                        ),
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'htmlOptions' => array('class' => 'pull-right'),
                            'items' => array(
                                array('icon' => 'i-tools', 'url' => Yii::app()->request->getBaseUrl(true) . '/painel/user/update', 'itemOptions' => array('class' => 'be-config')),
                                array('label' => 'Sair', 'url' => Yii::app()->request->getBaseUrl(true) . '/painel/user/logout'),
                            )
                        )
                    )
                ));
                ?>
            </div>
            <div class="container">
                <?php
                $this->widget('bootstrap.widgets.TbAlert', array(
                    'fade' => true,
                    'closeText' => '&times;',
                    'userComponentId' => 'user',
                ));
                ?>
            </div>
        <?php } ?>

        <div class="container" id="page">

            <div class="header">
                <div class="top visible-desktop <?php echo !Yii::app()->user->isGuest ? 'no-margin' : ''; ?>">
                    <?php if (!Yii::app()->user->isGuest) { ?>
                        <i class="i-beaker"></i>
                    <?php } ?>
                </div>
                <?php
                $model = Perfil::model()->findByPk(1);
                
                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'brand' => 'Lis Falkowski',
                    'brandOptions' => array(
                        'class' => 'visible-phone visible-tablet'
                    ),
                    'fixed' => false,
                    'collapse' => true,
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'items' => array(
                                array('label' => 'Home', 'url' => Yii::app()->request->getBaseUrl(true), 'itemOptions' => array('class' => 'visible-desktop')),
                                array('label' => 'Projetos', 'url' => Yii::app()->request->getBaseUrl(true) . '/projetos'),
                            )
                        ),
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'htmlOptions' => array('class' => 'pull-right visible-desktop'),
                            'items' => array(
                                array('icon' => 'i-mail', 'url' => 'mailto:' . $model->email, 'itemOptions' => array('class' => 'mail-icon')),
                                array('label' => $model->email, 'itemOptions' => array('class' => 'mail')),
                                array('icon' => 'i-phone', 'url' => 'tel:' . $model->getCleanPhone(), 'itemOptions' => array('class' => 'phone-icon')),
                                array('label' => $model->telefone, 'itemOptions' => array('class' => 'phone')),
                            )
                        )
                    )
                ));
                ?>
                <hr class="bottom" />
            </div>

            <div id="main">
                <?php echo $content; ?>
            </div>

            <div class="clear"></div>

        </div>
    </body>
</html>
