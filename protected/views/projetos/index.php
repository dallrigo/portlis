<?php if (isset($modelCategorias) && count($modelCategorias) > 1) { ?>
    <div id="filtro-projetos">
        <div class="filtro-projetos-inner">
            <?php foreach ($modelCategorias as $categoria) { ?>
                <?php echo $categoria->getIcon(); ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (isset($modelProjetos) && !empty($modelProjetos)) { ?>

    <div id="projetos">
        <div id="projetos-inner">
            <?php foreach ($modelProjetos as $projeto) { ?>
                <a data-categoria="<?php echo $projeto->categoria; ?>" data-nome="<?php echo $projeto->idCategoria->getNomeBase(); ?>" class="item" href="<?php echo Yii::app()->request->getBaseUrl(true) . '/projetos/' . $projeto->primaryKey; ?>">
                    <?php
                    echo Yii::app()->easyImage->thumbOf(ImagePath::fullPath(array(
                                'categoria' => $projeto->categoria,
                                'projeto' => $projeto->primaryKey
                                    ), true) . $projeto->capa . '.jpg', array(
                        'resize' => array('height' => 220),
                        'crop' => array('width' => 350, 'height' => 220)
                    ));
                    ?>
                    <div class="caption">
                        <p><?php echo $projeto->nome; ?></p>
                    </div>
                    <div class="item-hover"></div>
                </a>
            <?php } ?>
        </div>
    </div>

<?php } ?>

<script>
    $(document).ready(function() {
        $('.img-icon').not('.active').hover(
                function() {
                    $('img.top').animate({
                        opacity: 0
                    }, 20);
                },
                function() {
                    $('img.top').animate({
                        opacity: 1
                    }, 20);
                }
        );

        if ($('#filtro-projetos').width() < 404)
            $('.filtro-projetos-inner').addClass('filtro-mobile');

        $(window).resize(function() {
            if ($('#filtro-projetos').width() < 404)
                $('.filtro-projetos-inner').addClass('filtro-mobile');
            else
                $('.filtro-projetos-inner').removeClass('filtro-mobile');
        });
    });
</script>