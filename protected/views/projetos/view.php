<div id="projeto-view">
    <p class="nome">
        <span class="categoria-icon"><?php echo $model->idCategoria->getIcon(false); ?></span>
        <span class="projeto-nome <?php echo ($prev == '' && $next == '') ? 'full' : ''; ?>"><?php echo $model->nome; ?></span>
        <?php if ($prev != '' && $next != '') { ?>
            <a class="projeto-prev" href="<?php echo $prev; ?>"><i class="i-left-open-big"></i></a>
            <a class="projeto-next" href="<?php echo $next; ?>"><i class="i-right-open-big"></i></a>
        <?php } ?>
    </p>

    <?php if (isset($model->descricao) && $model->descricao != '') { ?>
        <p class="descricao">
            <?php echo $model->descricao; ?>
        </p>
    <?php } ?>

    <?php if ((isset($imagens) && !empty($imagens)) || (isset($model->capa) && $model->capa)) { ?>

        <div id="galleria">
            <?php if (isset($model->capaVideo) && $model->capaVideo) { ?>
                <a href="<?php echo $model->capaVideo; ?>"><img src="<?php echo $model->capa; ?>" data-title="<?php echo $model->capaNome; ?>" /></a>
            <?php } else { ?>
                <?php
                echo ImagePath::forGalleria(array(
                    'categoria' => $model->categoria,
                    'projeto' => $model->primaryKey,
                    'arquivo' => $model->capa,
                    'extensao' => $model->capaExtensao,
                    'nome' => $model->capaNome,
                ));
                ?>
            <?php } ?>
            <?php foreach ($imagens as $imagem) { ?>
                <?php if ($imagem->video === null) { ?>
                    <?php
                    echo ImagePath::forGalleria(array(
                        'categoria' => $model->categoria,
                        'projeto' => $model->primaryKey,
                        'arquivo' => $imagem->arquivo,
                        'extensao' => $imagem->extensao,
                        'nome' => $imagem->nome,
                    ));
                    ?>
                <?php } else { ?>
                    <a href="<?php echo $imagem->video; ?>"><img src="<?php echo $imagem->arquivo; ?>" data-title="<?php echo $imagem->nome; ?>" /></a>
                <?php } ?>
            <?php } ?>
        </div>

    <?php } else { ?>
        <div id="galleria" style="display: none;"></div>
    <?php } ?>

    <?php if (isset($model->texto) && $model->texto != '') { ?>
        <p class="texto">
            <?php echo $model->texto; ?>
        </p>
    <?php } ?>

</div>

<script>
    // Initialize Galleria
    Galleria.run('#galleria', {
//        extend: function(options) {
//            // listen to when an image is shown
//            this.bind('image', function(e) {
//                // lets make galleria open a lightbox when clicking the main image:
//                $(e.imageTarget).click(this.proxy(function() {
//                    this.enterFullscreen();
//                }));
//            });
//        }
    });

    Galleria.configure({
        'debug': false,
        'height': 450,
//        'autoplay': 2000,
        'lightbox': true,
        'imageCrop': false,
        'showInfo': false,
//        '_toggleInfo': false
    });

//    $('#galleria').data('galleria').enterFullscreen();
</script>