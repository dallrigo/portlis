<div id="profile-picture">
    <?php echo Yii::app()->easyImage->thumbOf(Yii::app()->theme->basePath . '/images/profile.png', array(), array('class' => 'img-circle')); ?>
</div>

<div id="profile-content">
    <span class="title"><?php echo $model->titulo; ?></span>
    <span class="cau">CAU <?php echo $model->cau; ?></span>
    <span class="description"><?php echo $model->descricao; ?></span>
    <span class="email visible-phone visible-tablet"><?php echo $model->email; ?><a href="mailto:<?php echo $model->email; ?>"><i class="i-mail"></i></a></span>
    <span class="phone visible-phone visible-tablet"><?php echo $model->telefone; ?><a href="tel:<?php echo $model->getCleanPhone(); ?>"><i class="i-phone"></i></a></span>
</div>