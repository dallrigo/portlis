<div class="backend">

    <div class="hero-unit">
        <h2>Error <?php echo $code; ?></h2>

        <p class="text-error">
            <?php echo CHtml::encode($message); ?>
        </p>
    </div>
</div>