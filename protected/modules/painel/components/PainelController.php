<?php

class PainelController extends GxController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
        );
    }

}
