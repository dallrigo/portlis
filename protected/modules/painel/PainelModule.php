<?php

class PainelModule extends CWebModule {

    public $defaultController = 'user';

    public function init() {
        
        Yii::app()->setComponent('user', array(
            'loginUrl' => Yii::app()->request->getBaseUrl(true) . '/painel/user/login'
        ));
    }

}
