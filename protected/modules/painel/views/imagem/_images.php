<div class="form-centered">
    <h1>Imagens</h1>

    <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
    <br />

    <div id="container">
        <a id="pickfiles" class="btn" href="javascript:;">Arquivos...</a>
        <a id="uploadfiles" class="btn btn-success" href="javascript:;">Upload</a>
    </div>

    <br />
    <div id="console"></div>

    <br />
    <h1>Vídeos</h1>

    <div id="videolist">
        <div class="videoitem" id="video_0">
            <input type="text" />
        </div>
    </div>
    <div>
        <a id="addvideos" class="btn" href="javascript:;">+</a>
        <a id="uploadvideos" class="btn btn-success" href="javascript:;">Salvar</a>
    </div>

</div>

<?php
$this->beginWidget('bootstrap.widgets.TbImageGallery', array(
    'id' => 'be-gallery',
    'fullScreen' => false
));

$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'type' => 'striped bordered',
    'id' => 'images-grid',
    'dataProvider' => $model->search(),
    'sortableRows' => true,
    'sortableAttribute' => 'ordem',
    'sortableAjaxSave' => true,
    'sortableAction' => 'painel/imagem/sortable',
    'ajaxUrl' => Yii::app()->request->getUrl(),
    'fixedHeader' => true,
    'columns' => array(
        array(
            'name' => 'ordem',
            'header' => '#',
            'htmlOptions' => array(
                'style' => 'width: 40px; text-align: center;'
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'nome',
            'sortable' => false,
            'editable' => array(
                'type' => 'text',
                'url' => Yii::app()->request->getBaseUrl(true) . '/painel/imagem/updateNome',
                'placement' => 'right',
                'inputclass' => 'span3'
            )
        ),
        array(
            'header' => 'Imagem',
            'type' => 'raw',
            'value' => function($data) use ($modelProjeto) {

        if ($data->video !== null) {
            echo '<a class="thumb video" href="' . $data->video . '" target="_blank">';
            echo '<img src="' . $data->arquivo . '" width="120px" />';
            echo '<i class="i-youtube"></i>';
            echo '</a>';
        } else {
            $thumbPath = ImagePath::fullPath(array('categoria' => $modelProjeto->categoria, 'projeto' => $modelProjeto->primaryKey), true) . $data->arquivo . '.jpg';
            $thumbUrl = ImagePath::fullUrl(array('categoria' => $modelProjeto->categoria, 'projeto' => $modelProjeto->primaryKey), true) . $data->arquivo . '.jpg';
            $imageUrl = ImagePath::fullUrl(array('categoria' => $modelProjeto->categoria, 'projeto' => $modelProjeto->primaryKey)) . $data->arquivo . '.' . $data->extensao;

            echo '<a data-gallery="gallery" class="thumb"'
            . ' data-download="' . $imageUrl . '"'
            . ' href="' . $thumbUrl . '"'
            . ' title="' . $data->nome . '"'
            . '>';
            echo '<img src="' . Yii::app()->easyImage->thumbSrcOf($thumbPath, array(
                'resize' => array('height' => 120),
                'quality' => 80,
            )) . '" width="120px" />';
            echo '</a>';
        }
    },
            'htmlOptions' => array('class' => 'no-move', 'nowrap' => 'nowrap', 'style' => 'text-align: center; width: 120px;')
        ),
        array(
            'htmlOptions' => array('class' => 'no-move', 'nowrap' => 'nowrap', 'style' => 'text-align:center;'),
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{open} {delete}',
            'buttons' => array(
                'open' => array(
                    'label' => '<i class="i-link-ext"></i>',
                    'url' => '$data->video === null ? ImagePath::fullUrl(array("categoria" => $data->idProjeto->categoria, "projeto" => $data->projeto)) . $data->arquivo . "." . $data->extensao : $data->video',
                    'options' => array(
                        'title' => 'Abrir original',
                        'target' => '_blank',
                        'style' => 'color: #151515; vertical-align: middle;'
                    ),
                ),
                'delete' => array(
                    'url' => 'Yii::app()->request->getBaseUrl(true) . "/painel/imagem/delete/id/$data->id"',
                    'options' => array(
                        'title' => 'Deletar'
                    ),
                ),
            )
        )
    ),
));
$this->endWidget();
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/plupload/plupload.full.min.js"); ?>
<script type="text/javascript">
    var uploader = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4',
        browse_button: 'pickfiles',
        container: document.getElementById('container'),
        url: '<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/imagem/upload'; ?>',
        flash_swf_url: '../js/Moxie.swf',
        silverlight_xap_url: '../js/Moxie.xap',
        chunk_size: '1mb',
        unique_names: true,
        filters: {
            max_file_size: '100mb',
            mime_types: [
                {title: "Image files", extensions: "jpg,jpeg,png"}
            ]
        },
        multipart_params: {
            categoria: '<?php echo $modelProjeto->categoria; ?>',
            projeto: '<?php echo $modelProjeto->primaryKey; ?>'
        },
        init: {
            PostInit: function() {
                document.getElementById('filelist').innerHTML = '';
                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },
            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    document.getElementById('filelist').innerHTML += '<div id="' + file.id + '" class="file_item"><i class="i-cancel-circled" data-id="' + file.id + '" onclick="removeFile(this);"></i><span>' + file.name + '</span> <div class="progress progress-striped"><div class="bar bar-success" style="width: 0;"></div></div></div>';
                });
            },
            BeforeUpload: function(up, file) {
                uploader.settings.multipart_params.nome = file.name;
            },
            UploadProgress: function(up, file) {
                $('#' + file.id).find('.bar').width(file.percent + '%');
            },
            UploadComplete: function(up, files) {
                $.fn.yiiGridView.update('images-grid');
            },
            Error: function(up, err) {
                document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
            }
        }
    });
    uploader.init();
    function removeFile(el) {
        uploader.removeFile($(el).data('id'));
        $('#filelist').find('#' + $(el).data('id')).remove();
    }

    // Vídeos
    $(document).ready(function() {

        $('#addvideos').on('click', function() {
            $('#videolist').append(function() {

                lastId = $('#videolist').find('.videoitem').last().attr('id');
                newId = parseInt(lastId.substring(6)) + 1;
                newId = 'video_' + newId.toString();

                str = '<div id="' + newId + '" class="videoitem">';
                str += '<input type="text" />';
                str += '<i class="i-cancel-circled" onclick="removeVideo(this);"></i>';
                str += '</div>';
                return str;
            });
        });

        $('#uploadvideos').on('click', function() {
            var videos = new Array();
            $('#videolist').find('.videoitem').each(function() {
                if (!$(this).hasClass('save') && $(this).find('input').val() !== '') {
                    url = $(this).find('input').val();
                    videoId = videoParser(url);

                    if (videoId !== false) {
                        videos.push({
                            videoId: videoId,
                            url: url,
                            domId: $(this).attr('id')
                        });
                    } else {
                        $(this).find('input').css('borderColor', '#da4f49');
                    }
                }
            });

            if (videos.length > 0) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/imagem/saveVideos'; ?>',
                    data: {videos: videos, projeto: '<?php echo $modelProjeto->primaryKey; ?>'},
                    success: function(data) {
                        obj = JSON.parse(data);

                        obj.forEach(function(item) {
                            videoThumb(item.id, item.videoId);
                            $('#videolist').find('#' + item.domId).addClass('save');
                            ('#videolist').find('#' + item.domId).find('input').css('borderColor', '#5bb75b');
                        });
                    }
                });
            }
        });

    });

    function removeVideo(el) {
        $(el).parent().remove();
    }

    function videoParser(url) {
        var regex = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
        var match = url.match(regex);
        if (match && match[1].length == 11) {
            return match[1];
        } else {
            return false;
        }
    }

    function videoThumb(id, videoId) {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/videos",
            data: "id=" + videoId
                    + "&key=AIzaSyDP_omtRSGFvT43szsAW4Pf29-FGG0SHxY"
                    + "&part=snippet",
            success: function(data) {
                console.log(data);
                thumb = data.items[0].snippet.thumbnails.high.url;
                title = data.items[0].snippet.title;

                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/imagem/saveVideoInfo'; ?>',
                    data: {id: id, nome: title, arquivo: thumb},
                    success: function(data) {
                        console.log(data);
                        $.fn.yiiGridView.update('images-grid');
                    }
                });
            }
        });
    }

</script>

<style>
    #filelist {
        display: block;
        border: 1px solid #dddddd;
        border-radius: 4px;
        padding: 0 10px;
    }

    #filelist .file_item {
        display: block;
        max-width: 700px;
        height: 30px;
        line-height: 30px;
        margin: 0 auto;
        padding: 5px;
    }

    #filelist .file_item span {
        display: inline-block;
        min-width: 300px;
        float: left;
        text-align: left;
    }

    #filelist .file_item i {
        display: inline-block;
        float: right;
        font-size: 18px;
        margin-top: 5px;
        margin-left: 5px;
        cursor: pointer;
        color: #da4f49;
    }

    #filelist .file_item .progress {
        background-image: linear-gradient(to bottom, #d0d0d0, #e0e0e0);
        margin: 5px 0 0;
    }

    #videolist {
        border: 1px solid #dddddd;
        border-radius: 4px;
        display: block;
        padding: 10px 10px 0;
        margin-bottom: 20px;
    }

    #videolist .videoitem {
        position: relative;
        display: block;
        max-width: 500px;
        margin: 0 auto;
    }

    #videolist .videoitem input {
        display: inline-block;
        width: 300px;
    }

    #videolist .videoitem i {
        position: absolute;
        top: 5px;
        right: 62px;
        font-size: 18px;
        margin-left: 5px;
        cursor: pointer;
        color: #da4f49;
    }
</style>