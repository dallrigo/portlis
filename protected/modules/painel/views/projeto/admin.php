<div class="backend">
    <p class="header no-link">
        <span class="be-icon"><i class="i-wallet"></i></span>
        <span class="be-operation">Projetos</span>
    </p>

    <?php
    $this->beginWidget('bootstrap.widgets.TbImageGallery', array(
        'id' => 'be-gallery',
        'fullScreen' => false
    ));

    $this->widget('bootstrap.widgets.TbGroupGridView', array(
        'type' => 'striped bordered',
        'id' => 'projetos-grid',
        'dataProvider' => $model->search(),
        'mergeColumns' => array('categoria'),
        'columns' => array(
            array(
                'name' => 'categoria',
                'value' => '$data->idCategoria->nome',
                'htmlOptions' => array(
                    'style' => 'width: 150px;'
                )
            ),
            'nome',
            array(
                'header' => 'Imagem',
                'type' => 'raw',
                'value' => function($data) {

            if (isset($data->capaVideo) && $data->capaVideo) {
                echo '<a class="thumb video" href="' . $data->capaVideo . '" target="_blank" data-toggle="tooltip" data-placement="top" title="' . $data->capaNome . '">';
                echo '<img src="' . $data->capa . '" width="120px" />';
                echo '<i class="i-youtube"></i>';
                echo '</a>';
            } else {
                $thumbPath = ImagePath::fullPath(array('categoria' => $data->categoria, 'projeto' => $data->primaryKey), true) . $data->capa . '.jpg';
                $thumbUrl = ImagePath::fullUrl(array('categoria' => $data->categoria, 'projeto' => $data->primaryKey), true) . $data->capa . '.jpg';
                $imageUrl = ImagePath::fullUrl(array('categoria' => $data->categoria, 'projeto' => $data->primaryKey)) . $data->capa . '.' . $data->capaExtensao;
                
                echo '<a data-gallery="gallery" class="thumb" data-toggle="tooltip" data-placement="top"'
                . ' data-download="' . $imageUrl . '"'
                . ' href="' . $thumbUrl . '"'
                . ' title="' . $data->capaNome . '"'
                . '>';
                echo '<img src="' . Yii::app()->easyImage->thumbSrcOf($thumbPath, array(
                    'resize' => array('height' => 120),
                    'quality' => 80,
                )) . '" width="120px" />';
                echo '</a>';
            }
        },
                'htmlOptions' => array('class' => 'no-move', 'nowrap' => 'nowrap', 'style' => 'text-align: center; width: 120px;')
            ),
            array(
                'class' => 'bootstrap.widgets.TbToggleColumn',
                'toggleAction' => '//painel/projeto/toggle',
                'name' => 'ativo',
                'checkedButtonLabel' => 'Ativo',
                'uncheckedButtonLabel' => 'Clique para ativar',
                'htmlOptions' => array(
                    'style' => 'text-align: center; vertical-align: middle; width: 42px;'
                )
            ),
            array(
                'htmlOptions' => array('nowrap' => 'nowrap', 'style' => 'text-align:center;'),
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{view} {preview} {update} {delete}',
                'buttons' => array(
                    'view' => array(
                        'url' => 'Yii::app()->request->getBaseUrl(true) . "/projetos/" . $data->primaryKey',
                        'visible' => '$data->ativo == 1 ? true : false'
                    ),
                    'preview' => array(
                        'url' => 'Yii::app()->request->getBaseUrl(true) . "/painel/projeto/update/id/" . $data->primaryKey . "/op/preview"',
                        'visible' => '$data->ativo == 0 ? true : false',
                        'label' => 'View',
                        'icon' => 'icon-eye-open',
                    ),
                )
            ),
        ),
    ));
    $this->endWidget();
    ?>
</div>