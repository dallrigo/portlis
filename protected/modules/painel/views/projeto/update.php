<div class="backend">

    <p class="header <?php echo $model->ativo == 1 ? 'no-preview' : ''; ?>">
        <span class="be-icon"><i class="i-note"></i></span>
        <span class="be-operation">Gerenciar Projeto</span>
        <a class="be-form <?php echo $operation == 'form' ? 'active' : ''; ?>" href="<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/projeto/update/id/' . $model->primaryKey . '/op/form'; ?>"><i class="i-pencil"></i></a>
        <a class="be-images <?php echo $operation == 'images' ? 'active' : ''; ?>" href="<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/projeto/update/id/' . $model->primaryKey . '/op/images'; ?>"><i class="i-photo"></i></a>
        <?php if ($model->ativo == 0) { ?>
            <a class="be-preview <?php echo $operation == 'preview' ? 'active' : ''; ?>" href="<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/projeto/update/id/' . $model->primaryKey . '/op/preview'; ?>"><i class="i-eye"></i></a>
            <?php } ?>
    </p>

    <?php
    if ($operation == 'form') {
        $this->renderPartial('_form', array(
            'model' => $model,
        ));
    } else if ($operation == 'images') {
        $this->renderPartial('painel.views.imagem._images', array(
            'model' => $modelImagem,
            'modelProjeto' => $model,
        ));
    } else if ($operation == 'preview') {
        $this->renderPartial('_preview', array(
            'model' => $model,
            'imagens' => $imagens,
        ));
    }
    ?>

</div>