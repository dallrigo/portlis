<div class="form">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'projeto-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <?php echo $form->dropDownListRow($model, 'categoria', GxHtml::listDataEx(Categoria::model()->findAllAttributes(null, true))); ?>
    </div><!-- row -->

    <div class="row-fluid">
        <?php echo $form->textFieldRow($model, 'nome'); ?>
    </div><!-- row -->
    <div class="row-fluid">
        <?php echo $form->html5EditorRow($model, 'descricao', array('lang' => 'pt-BR', 'height' => '100px')); ?>
    </div><!-- row -->
    <div class="row-fluid">
        <?php echo $form->html5EditorRow($model, 'texto', array('lang' => 'pt-BR', 'height' => '300px')); ?>
    </div><!-- row -->

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'success',
            'label' => 'Salvar'
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'url' => Yii::app()->request->getBaseUrl(),
            'type' => 'danger',
            'label' => 'Cancelar'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->