<div class="backend">

    <p class="header">
        <span class="be-icon"><i class="i-note"></i></span>
        <span class="be-operation">Incluir Projeto</span>
        <a class="be-form active" href="<?php echo Yii::app()->getBaseUrl(true) . '/painel/projeto/create'; ?>"><i class="i-pencil"></i></a>
        <a class="be-images submit" href="javascript:void();"><i class="i-photo"></i></a>
        <a class="be-preview submit" href="javascript:void();"><i class="i-eye"></i></a>
    </p>

    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'buttons' => 'create'));
    ?>

</div>

<script>
    $(document).ready(function() {
        $('.submit').on('click', function() {
            $('#projeto-form').submit();
        });
    });
</script>