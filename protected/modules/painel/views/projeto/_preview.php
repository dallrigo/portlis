<div id="projeto-ativar">
    <a id="ativarprojeto" href="javascript:;" class="btn btn-success btn-large">Ativar projeto</a><span>&nbsp;<i class="i-lock"></i>&nbsp;</span>
</div>

<div id="projeto-view">
    <p class="nome">
        <span class="categoria-icon"><?php echo $model->idCategoria->getIcon(false); ?></span>
        <span class="projeto-nome full"><?php echo $model->nome; ?></span>
    </p>

    <?php if (isset($model->descricao) && $model->descricao != '') { ?>
        <p class="descricao">
            <?php echo $model->descricao; ?>
        </p>
    <?php } ?>

    <?php if ((isset($imagens) && !empty($imagens)) || (isset($model->capa) && $model->capa)) { ?>

        <div id="galleria">
            <?php if (isset($model->capaVideo) && $model->capaVideo) { ?>
                <a href="<?php echo $model->capaVideo; ?>"><img src="<?php echo $model->capa; ?>" data-title="<?php echo $model->capaNome; ?>" /></a>
            <?php } else { ?>
                <?php
                echo ImagePath::forGalleria(array(
                    'categoria' => $model->categoria,
                    'projeto' => $model->primaryKey,
                    'arquivo' => $model->capa,
                    'extensao' => $model->capaExtensao,
                    'nome' => $model->capaNome,
                ));
                ?>
            <?php } ?>
            <?php foreach ($imagens as $imagem) { ?>
                <?php if ($imagem->video === null) { ?>
                    <?php
                    echo ImagePath::forGalleria(array(
                        'categoria' => $model->categoria,
                        'projeto' => $model->primaryKey,
                        'arquivo' => $imagem->arquivo,
                        'extensao' => $imagem->extensao,
                        'nome' => $imagem->nome,
                    ));
                    ?>
                <?php } else { ?>
                    <a href="<?php echo $imagem->video; ?>"><img src="<?php echo $imagem->arquivo; ?>" data-title="<?php echo $imagem->nome; ?>" /></a>
                <?php } ?>
            <?php } ?>
        </div>

    <?php } else { ?>
        <div id="galleria" style="display: none;"></div>
    <?php } ?>

    <?php if (isset($model->texto) && $model->texto != '') { ?>
        <p class="texto">
            <?php echo $model->texto; ?>
        </p>
    <?php } ?>

</div>

<script>
    // Initialize Galleria
    Galleria.run('#galleria', {
//        extend: function(options) {
//            // listen to when an image is shown
//            this.bind('image', function(e) {
//                // lets make galleria open a lightbox when clicking the main image:
//                $(e.imageTarget).click(this.proxy(function() {
//                    this.enterFullscreen();
//                }));
//            });
//        }
    });

    Galleria.configure({
        'debug': false,
        'height': 450,
//        'autoplay': 2000,
        'lightbox': true,
        'imageCrop': false,
        'showInfo': false,
//        '_toggleInfo': false
    });

    $(document).ready(function() {
        $('#ativarprojeto').not('.ativo').on('click', function() {
            $('#projeto-ativar').find('i').removeClass('i-lock').addClass('i-spin5').addClass('animate-spin');

            $.ajax({
                url: '<?php echo Yii::app()->request->getBaseUrl(true) . '/painel/projeto/ativarProjeto/id/' . $model->primaryKey; ?>',
                data: '',
                success: function(data) {
                    $('#projeto-ativar').find('i').removeClass('i-spin5').removeClass('animate-spin').addClass('i-thumbs-up');
                    $('#ativarprojeto').addClass('ativo');
                    $('#ativarprojeto').text('Projeto ativo');
                }
            });
        });
    });

</script>

<style>
    #projeto-ativar {
        display: block;
        text-align: center;
        margin-bottom: 40px;
    }

    #projeto-ativar a {
        outline: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    #projeto-ativar span {
        border-radius: 6px;
        font-size: 17.5px;
        padding: 11px 19px;

        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-bottom-color: none;
        border-left-color: none;
        border-right-color: none;
        border-top-color: none;

        background-color: #f5f5f5;
        background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
        background-repeat: repeat-x;
        border-color: #cccccc #cccccc #b3b3b3;
        border-image: none;
        border-radius: 4px;
        border-style: solid;
        border-width: 1px;
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
        color: #333333;
        display: inline-block;
        line-height: 20px;
        margin-bottom: 0;
        text-align: center;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
        vertical-align: middle;

        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }

</style>