<div class="form-centered">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'upload-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => '&times;',
        'userComponentId' => 'user',
    ));
    ?>
    
    <?php echo $form->fileField($model, 'picture'); ?>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'success',
            'label' => 'Salvar'
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'url' => Yii::app()->request->getBaseUrl(true),
            'type' => 'danger',
            'label' => 'Cancelar'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>