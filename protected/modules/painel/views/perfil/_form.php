<div class="form">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'perfil-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div id="profile-picture" style="margin-bottom: 20px;">
        <?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/profile.png', '', array('class' => 'img-circle')); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->fileFieldRow($model, 'picture'); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->textFieldRow($model, 'titulo'); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->html5EditorRow($model, 'descricao', array('lang' => 'pt-BR', 'height' => '100px')); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->textFieldRow($model, 'cau'); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->maskedTextFieldRow($model, 'telefone', array('mask' => '(99) 9999-9999')); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->textFieldRow($model, 'email'); ?>
    </div>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'success',
            'label' => 'Salvar'
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'url' => Yii::app()->request->getBaseUrl(true),
            'type' => 'danger',
            'label' => 'Cancelar'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div>
