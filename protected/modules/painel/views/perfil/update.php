<div class="backend">

    <p class="header no-link">
        <span class="be-icon"><i class="i-user"></i></span>
        <span class="be-operation">Editar Perfil</span>
    </p>

    <?php
    $this->renderPartial('_form', array(
        'model' => $model
    ));
    ?>

</div>