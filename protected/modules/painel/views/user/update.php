<div class="backend">

    <p class="header no-link">
        <span class="be-icon"><i class="i-tools"></i></span>
        <span class="be-operation">Editar Usuário</span>
    </p>

    <?php
    $this->renderPartial('_form', array(
        'model' => $model
    ));
    ?>

</div>