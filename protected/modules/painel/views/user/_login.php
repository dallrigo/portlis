<div class="form-centered">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-login-form',
        'focus' => array($model, 'username'),
    ));
    ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => '&times;',
        'userComponentId' => 'user',
    ));
    ?>

    <?php if ($ev == 'update') { ?>
        <div class="alert in alert-block fade alert-success">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            Senha alterada com sucesso, por favor relogue.
        </div>
    <?php } ?>

    <?php echo $form->textFieldRow($model, 'username', array('class' => 'span3')); ?>
    <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span3')); ?>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => 'Login'
        ));
        ?>
    </div>

    <?php
    $this->endWidget();
    unset($form);
    ?>
</div>