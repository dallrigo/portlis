<div class="form">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => false,
        'focus' => array($model, 'oldPassword'),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <?php echo $form->textFieldRow($model, 'username', array('disabled' => 'disabled')); ?>
    </div><!-- row -->

    <div class="row-fluid">
        <?php echo $form->passwordFieldRow($model, 'oldPassword'); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->passwordFieldRow($model, 'newPassword'); ?>
    </div>

    <div class="row-fluid">
        <?php echo $form->passwordFieldRow($model, 'newPasswordConfirm'); ?>
    </div>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'success',
            'label' => 'Salvar'
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'url' => Yii::app()->request->getBaseUrl(),
            'type' => 'danger',
            'label' => 'Cancelar'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->

<style>
    #user-form label.control-label {
        width: 165px;
    }
</style>