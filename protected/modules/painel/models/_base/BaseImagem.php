<?php

/**
 * This is the model base class for the table "imagem".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Imagem".
 *
 * Columns in table "imagem" available as properties of the model,
 * followed by relations of table "imagem" available as properties of the model.
 *
 * @property integer $id
 * @property string $nome
 * @property string $descricao
 * @property string $arquivo
 * @property string $extensao
 * @property string $video
 * @property integer $ordem
 * @property integer $projeto
 *
 * @property Projeto $idProjeto
 */
abstract class BaseImagem extends GxActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'imagem';
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Imagem|Imagems', $n);
    }

    public static function representingColumn() {
        return 'nome';
    }

    public function rules() {
        return array(
            array('ordem, projeto', 'numerical', 'integerOnly' => true),
            array('nome, arquivo, video', 'length', 'max' => 120),
            array('extensao', 'length', 'max' => 5),
            array('descricao', 'safe'),
            array('nome, descricao, arquivo, extensao, video, ordem, projeto', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, nome, descricao, arquivo, extensao, video, ordem, projeto', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'idProjeto' => array(self::BELONGS_TO, 'Projeto', 'projeto'),
        );
    }

    public function pivotModels() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
            'descricao' => Yii::t('app', 'Descricao'),
            'arquivo' => Yii::t('app', 'Arquivo'),
            'extensao' => Yii::t('app', 'Extensão'),
            'video' => Yii::t('app', 'Vídeo'),
            'ordem' => Yii::t('app', 'Ordem'),
            'projeto' => null,
            'idProjeto' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('descricao', $this->descricao, true);
        $criteria->compare('arquivo', $this->arquivo, true);
        $criteria->compare('extensao', $this->extensao, true);
        $criteria->compare('video', $this->video, true);
        $criteria->compare('ordem', $this->ordem);
        $criteria->compare('projeto', $this->projeto);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'ordem ASC',
            ),
        ));
    }
    
}
