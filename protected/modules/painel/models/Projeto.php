<?php

Yii::import('painel.models._base.BaseProjeto');

class Projeto extends BaseProjeto {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getPrevId($id, $idCategoria = null, $nomeCategoria = '') {
        $url = Yii::app()->request->getBaseUrl(true) . '/projetos/';
        $condition = 'ativo = 1';

        if ($idCategoria != null) {
            $url .= $nomeCategoria . '/';
            $condition .= ' AND categoria = ' . $idCategoria;
        }

        $model = self::model()->find(array(
            'select' => 'id',
            'condition' => $condition . ' AND id < ' . $id,
            'order' => 'id DESC'
        ));

        if ($model)
            return $url . $model->id;
        else {
            $model = self::model()->find(array(
                'select' => 'id',
                'condition' => $condition . ' AND id != ' . $id,
                'order' => 'id DESC',
            ));
            if ($model)
                return $url . $model->id;
            else
                return '';
        }
    }

    public static function getNextId($id, $idCategoria = null, $nomeCategoria = '') {
        $url = Yii::app()->request->getBaseUrl(true) . '/projetos/';
        $condition = 'ativo = 1';
        
        if ($idCategoria != null) {
            $url .= $nomeCategoria . '/';
            $condition .= ' AND categoria = ' . $idCategoria;
        }
        
        $model = self::model()->find(array(
            'select' => 'id',
            'condition' => $condition . ' AND id > ' . $id,
            'order' => 'id ASC'
        ));

        if ($model)
            return $url . $model->id;
        else {
            $model = self::model()->find(array(
                'select' => 'id',
                'condition' => $condition. ' AND id != ' . $id,
                'order' => 'id ASC',
            ));
            if ($model)
                return $url . $model->id;
            else
                return '';
        }
    }

}
