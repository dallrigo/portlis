<?php

Yii::import('painel.models._base.BaseImagem');

class Imagem extends BaseImagem {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getOrdem($id) {
        $model = self::model()->find(array(
            'select' => 'ordem',
            'condition' => 'projeto = ' . $id,
            'order' => 'ordem DESC',
        ));
        if ($model)
            return $model->ordem + 1;
        else
            return 0;
    }
    
}
