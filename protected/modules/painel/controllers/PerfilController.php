<?php

class PerfilController extends GxController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
        );
    }

    public function actionUpdate() {
        $model = $this->loadModel(1, 'Perfil');
        $user = Yii::app()->getComponent('user');

        if (isset($_POST['Perfil'])) {
            $model->setAttributes($_POST['Perfil']);

            if ($model->save() && isset($_FILES['Perfil']['name']['picture']) && $_FILES['Perfil']['name']['picture'] != '') {
                $model->picture = $_POST['Perfil']['picture'];
                $model->picture = CUploadedFile::getInstance($model, 'picture');
                $image = getimagesize($model->picture->tempName);

                if ($image[0] == 250 && $image[1] == 250 && $image[2] == IMAGETYPE_PNG) {
                    if ($model->save()) {
                        $model->picture->saveAs(Yii::app()->theme->basePath . '/images/profile.png');
                    }
                } else {
                    $user->setFlash('error', 'Arquivo deve ser .png e 250x250');
                }
            }
            $user->setFlash('success', 'Perfil alterado com sucesso');
            $this->redirect(Yii::app()->request->getBaseUrl(true));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

}
