<?php

class ProjetoController extends GxController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
        );
    }

    public function actionCreate() {
        $model = new Projeto;
        $user = Yii::app()->getComponent('user');

        if (isset($_POST['Projeto'])) {
            $model->setAttributes($_POST['Projeto']);

            if ($model->save()) {

                $dir = realpath(Yii::app()->basePath . '/../images') . DIRECTORY_SEPARATOR . 'projetos' . DIRECTORY_SEPARATOR . $model->categoria . DIRECTORY_SEPARATOR . $model->primaryKey;

                if (!is_dir($dir)) {
                    mkdir($dir);
                }

                $user->setFlash('success', $model->label() . ' adicionado com sucesso');
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/painel/projeto/update/id/' . $model->primaryKey . '/op/images');
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id, $op = 'form') {
        $model = $this->loadModel($id, 'Projeto');
        $user = Yii::app()->getComponent('user');

        if ($op == 'form') {
            if (isset($_POST['Projeto'])) {
                $model->setAttributes($_POST['Projeto']);

                if ($model->save()) {
                    $user->setFlash('success', $model->label() . ' alterado com sucesso');
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/painel/projeto/update/id/' . $model->primaryKey . '/op/images');
                }
            }
        } else if ($op == 'images') {
            $modelImagem = new Imagem('search');
            $modelImagem->projeto = $model->primaryKey;
            if (isset($_GET['Imagem']))
                $modelImagem->setAttributes($_GET['Imagem']);
        } else if ($op == 'preview') {
            $imagens = Imagem::model()->findAll(array(
                'condition' => 'projeto = ' . $id . ' AND ordem != 0',
                'order' => 'ordem ASC'
            ));
        }


        $this->render('update', array(
            'model' => $model,
            'modelImagem' => isset($modelImagem) ? $modelImagem : null,
            'imagens' => isset($imagens) ? $imagens : null,
            'operation' => $op,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Projeto');

            if ($model) {
                $imagePath = ImagePath::fullPath(array('categoria' => $model->categoria, 'projeto' => $model->primaryKey));
                $thumbPath = $imagePath . 'thumb' . DIRECTORY_SEPARATOR;

                if (isset($model->imagens) && !empty($model->imagens)) {
                    foreach ($model->imagens as $imagem) {
                        //deleta arquivo
                        if ($imagem->video === null) {
                            //principal
                            unlink($imagePath . $imagem->arquivo . '.' . $imagem->extensao);

                            //thumb
                            unlink($thumbPath . $imagem->arquivo . '.jpg');
                        }
                        //deleta registro imagem
                        $imagem->delete();
                    }
                    rmdir($thumbPath);
                }
                if (!is_dir($thumbPath))
                    rmdir($imagePath);

                $model->delete();
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/painel/projeto/admin');
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionAtivarProjeto($id) {
        $model = Projeto::model()->findByPk($id);

        if ($model) {
            $model->ativo = 1;
            $model->save();
        }
    }

    public function actionAdmin() {
        $model = new Projeto('search');
        $model->unsetAttributes();

        if (isset($_GET['Projeto']))
            $model->setAttributes($_GET['Projeto']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actions() {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'Projeto',
            )
        );
    }

}
