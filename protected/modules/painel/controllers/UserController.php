<?php

class UserController extends GxController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('login'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('update'),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('?'),
            ),
        );
    }

    public function actionIndex() {
        if (Yii::app()->user->isGuest)
            $this->actionLogin();
        else
            $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionUpdate() {
        $model = $this->loadModel(1, 'User');
        $user = Yii::app()->getComponent('user');

        if (isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);
            $model->oldPassword = md5($model->oldPassword);

            if ($model->validate()) {
                $model->password = md5($model->newPassword);
                $model->setScenario('insert');
                $model->save();
                Yii::app()->user->logout();
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/painel/user/login/ev/update');
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionLogin($ev = '') {
        $model = new User;
        $user = Yii::app()->getComponent('user');

        if (isset($_POST['User'])) {
            $identity = new UserIdentity($_POST['User']['username'], $_POST['User']['password']);

            if ($identity->authenticate()) {
                Yii::app()->user->login($identity);
                $this->redirect(Yii::app()->request->getBaseUrl(true));
            } else {
                $user->setFlash('error', 'Usuário ou senha inválidos');
                $_POST['User']['password'] = '';
            }
        }

        $this->render('_login', array(
            'model' => $model,
            'ev' => $ev,
        ));
    }

    public function actionLogout() {
        if (!Yii::app()->user->isGuest)
            Yii::app()->user->logout();

        $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

}
