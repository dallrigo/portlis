<?php

class ImagemController extends GxController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
        );
    }

    public function actionUpdateNome() {
        if (isset($_POST['pk'])) {
            $model = Imagem::model()->findByPk($_POST['pk']);

            $model->nome = $_POST['value'];

            if ($model->save()) {
                return true;
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Imagem');

            if ($model) {
                $projeto = $model->projeto;

                //deleta arquivo
                if ($model->video === null) {
                    //principal
                    $imagePath = ImagePath::fullPath(array(
                        'categoria' => $model->idProjeto->categoria,
                        'projeto' => $model->projeto
                    ));
                    unlink($imagePath . $model->arquivo . '.' . $model->extensao);
                
                    //thumb
                    $thumbPath = $imagePath . 'thumb' . DIRECTORY_SEPARATOR;
                    unlink($thumbPath . $model->arquivo . '.jpg');
                }
                
                //organiza ordem
                $nextModels = Imagem::model()->findAll(array(
                    'condition' => 'projeto = ' . $projeto . ' AND ordem > ' . $model->ordem,
                    'order' => 'ordem ASC',
                ));

                if ($nextModels && !empty($nextModels)) {
                    foreach ($nextModels as $next) {
                        $next->ordem = $next->ordem - 1;
                        $next->save();
                    }
                }

                //deleta registro
                $model->delete();

                if (!Yii::app()->getRequest()->getIsAjaxRequest())
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/painel/projeto/id/' . $projeto . '/op/images');
            }
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionUpload() {
        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Settings
        //$targetDir = realpath(Yii::app()->basePath . '/../images') . DIRECTORY_SEPARATOR . 'projetos' . DIRECTORY_SEPARATOR . $_REQUEST['categoria'] . DIRECTORY_SEPARATOR . $_REQUEST['projeto'];
        $targetDir = ImagePath::fullPath(array(
                    'categoria' => $_REQUEST['categoria'],
                    'projeto' => $_REQUEST['projeto']
        ));

        $thumbDir = $targetDir . 'thumb' . DIRECTORY_SEPARATOR;

        //$targetDir = 'uploads';
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds
        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        // Create thumb dir
        if (!file_exists($thumbDir)) {
            @mkdir($thumbDir);
        }

        // Get a file name
        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        // Remove old temp files	
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }

        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off 
            rename("{$filePath}.part", $filePath);

            //nome do arquivo original
            $nomeInfo = new SplFileInfo($_REQUEST['nome']);
            $nomeExtension = $nomeInfo->getExtension();
            $nomeTitle = $nomeInfo->getBasename('.' . $nomeExtension);

            //nome do arquivo salvo
            $fileInfo = new SplFileInfo($fileName);
            $fileExtension = $fileInfo->getExtension();
            $fileTitle = $fileInfo->getBasename('.' . $fileExtension);

            // Salvar registro no banco
            $model = new Imagem;
            $model->projeto = $_REQUEST['projeto'];
            $model->arquivo = $fileTitle;
            $model->extensao = $fileExtension;
            $model->nome = $nomeTitle;
            $model->ordem = Imagem::getOrdem($model->projeto);
            $model->save();

            $thumb = new EasyImage($targetDir . $model->arquivo . '.' . $model->extensao);

            if ($thumb->image()->width > 1800)
                $thumb->resize(1800);

            $thumb->save($thumbDir . $model->arquivo . '.jpg', 100);
        }

        // Return Success JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
    }

    public function actionSaveVideos() {
        if (isset($_POST['videos']) && !empty($_POST['videos'])) {

            $result = array();

            foreach ($_POST['videos'] as $video) {
                $model = new Imagem;
                $model->projeto = $_POST['projeto'];
                $model->video = $video['url'];
                $model->ordem = Imagem::getOrdem($model->projeto);

                if ($model->save()) {
                    $result[] = array(
                        'id' => $model->primaryKey,
                        'videoId' => $video['videoId'],
                        'domId' => $video['domId'],
                    );
                }
            }

            echo CJSON::encode($result);
        }
    }

    public function actionSaveVideoInfo() {
        if (isset($_POST['id']) && $_POST['id']) {
            $result = '';

            $model = Imagem::model()->findByPk($_POST['id']);

            if ($model) {
                $model->nome = $_POST['nome'];
                $model->arquivo = $_POST['arquivo'];
                $model->save();
                $result = $model->primaryKey;
            }

            echo CJSON::encode($result);
        }
    }

    public function actions() {
        return array(
            'sortable' => array(
                'class' => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Imagem',
            ),
        );
    }

}
