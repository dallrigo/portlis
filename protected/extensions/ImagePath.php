<?php

/*
 * a href = principal / meio
 * img src = thumb / embaixo
 * img data-big = modal / fs
 */

class ImagePath {

    public static function basePath() {
        return realpath(Yii::app()->basePath . '/../images');
    }

    /*
     * $options array: [categoria] [projeto]
     * $thumb boolean
     */

    public static function fullPath($options, $thumb = false) {
        $categoria = $options['categoria'];
        $projeto = $options['projeto'];

        $root = self::basePath() . DIRECTORY_SEPARATOR;

        $imageUrl = $root . 'projetos'
                . DIRECTORY_SEPARATOR . $categoria
                . DIRECTORY_SEPARATOR . $projeto
                . DIRECTORY_SEPARATOR;

        if ($thumb)
            $imageUrl .= 'thumb' . DIRECTORY_SEPARATOR;

        return $imageUrl;
    }

    /*
     * $options array: [categoria] [projeto]
     * $thumb boolean
     */

    public static function fullUrl($options, $thumb = false) {
        $categoria = $options['categoria'];
        $projeto = $options['projeto'];

        $imageUrl = Yii::app()->request->baseUrl . '/images/projetos/' . $categoria . '/' . $projeto . '/';

        if ($thumb)
            $imageUrl .= 'thumb/';

        return $imageUrl;
    }

    /*
     * $options array: [categoria] [projeto] [arquivo] [extensao] [nome]
     */

    public static function forGalleria($options) {

        $imageUrl = self::fullUrl(array(
                    'categoria' => $options['categoria'],
                    'projeto' => $options['projeto']
                )) . $options['arquivo'] . '.' . $options['extensao'];

        $thumbFolder = self::fullPath(array(
                    'categoria' => $options['categoria'],
                    'projeto' => $options['projeto']
                        ), true);

        $thumbPath = $thumbFolder . $options['arquivo'] . '.jpg';
        $thumbUrl = self::fullUrl(array(
                    'categoria' => $options['categoria'],
                    'projeto' => $options['projeto']
                        ), true) . $options['arquivo'] . '.jpg';

        $title = "<a href='" . $imageUrl . "'"
                . " target='_blank'"
                . " data-toggle='tooltip'"
                . " title='Tamanho original'"
                . " data-placement='right'"
                . "><i class='i-link-ext'></i></a>" . $options['nome'];

        $thumbHref = Yii::app()->easyImage->thumbSrcOf($thumbPath, array(
            'resize' => array('height' => 380),
            'quality' => 90,
        ));

        $thumbSrc = Yii::app()->easyImage->thumbSrcOf($thumbPath, array(
            'resize' => array('height' => 40),
            'quality' => 90,
        ));

        $result = '<a href="' . $thumbHref . '">'
                . '<img'
                . ' src="' . $thumbSrc . '"'
                . ' data-big="' . $thumbUrl . '"'
                . ' data-title="' . $title . '"'
                . '/>'
                . '</a>';

        return $result;
    }

}
