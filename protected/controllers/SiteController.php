<?php

class SiteController extends Controller {

    public function actionIndex() {
        $model = Perfil::model()->findByPk(1);

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
}
