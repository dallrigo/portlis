<?php

class ProjetosController extends GxController {

    public function actionIndex() {
        $modelCategorias = Categoria::model()->findAll(array(
            'distinct' => true,
            'join' => 'inner join projeto p on p.categoria = t.id',
            'condition' => 'p.ativo = 1',
            'order' => 'nome ASC',
        ));

        $modelProjetos = Projeto::model()->findAll(array(
            'condition' => 'ativo = 1',
            'order' => 'categoria ASC, nome ASC',
        ));

        $this->render('index', array(
            'modelCategorias' => $modelCategorias,
            'modelProjetos' => $modelProjetos,
        ));
    }

    public function actionView($categoria = '', $id) {
        $model = Projeto::model()->findByPk($id);
        
        if ($model && $model->ativo == 1) {

            $imagens = Imagem::model()->findAll(array(
                'condition' => 'projeto = ' . $id . ' AND ordem != 0',
                'order' => 'ordem ASC'
            ));

            $idCategoria = null;
            if ($categoria != '')
                $idCategoria = $model->categoria;
            
            $this->render('view', array(
                'model' => $model,
                'imagens' => $imagens,
                'prev' => Projeto::getPrevId($id, $idCategoria, $categoria),
                'next' => Projeto::getNextId($id, $idCategoria, $categoria),
            ));
        } else {
            $this->redirect(Yii::app()->request->getBaseUrl(true) . '/projetos');
        }
    }

}
