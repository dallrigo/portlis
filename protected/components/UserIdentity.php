<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate() {

        $model = User::model()->findByAttributes(array('username' => $this->username));

        if ($model == null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($model->password !== md5($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $model->id;
            $this->errorCode = self::ERROR_NONE;
            $model->lastlogin = date('Y-m-d H:i:s');
            $model->save();
        }
        return !$this->errorCode;
    }
    
    public function getId() {
        return $this->_id;
    }

}
